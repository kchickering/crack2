#include "md5.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <limits.h>

/*
 * Hash table stuff
 * Followed implementation from here: https://gist.github.com/tonious/1377667
 */

typedef struct hash_entry_s {
	char *key;
	char *value;
	struct hash_entry_s *next;
} hash_entry_t;

typedef struct hashtable_s {
	int size;
	hash_entry_t **table;
} hashtable_t;

hashtable_t *hashtable_create (int size)
{
	hashtable_t *hashtable;

	if (!(hashtable = malloc(sizeof(hashtable_t)))) {
		return NULL;
	}

	if (!(hashtable->table = malloc(sizeof(hash_entry_t*) * size))) {
		return NULL;
	}

	for (int ii = 0; ii < size; ii++) {
		hashtable->table[ii] = NULL;
	}

	hashtable->size = size;

	return hashtable;
}

int hashtable_hash (hashtable_t *hashtable, char *key)
{
	unsigned long int hashvalue = 0;
	int ii = 0;

	while (hashvalue < ULONG_MAX && ii < strlen(key)) {
		hashvalue = hashvalue << 8;
		hashvalue += key[ii++];
	}

	return hashvalue % hashtable->size;
}

hash_entry_t *hashtable_newpair (char *key, char*value)
{
	hash_entry_t *newpair;

	if (!(newpair = malloc(sizeof(hash_entry_t)))) {
		return NULL;
	}

	if (!(newpair->key = strdup(key))) {
		return NULL;
	}

	if (!(newpair->value = strdup(value))) {
		return NULL;
	}

	return newpair;
}

void hashtable_set (hashtable_t *hashtable, char *key, char *value)
{
	int bin = 0;
	hash_entry_t *newpair, *next, *last;

	bin = hashtable_hash(hashtable, key);
	next = hashtable->table[bin];

	while (next && next->key && strcmp(key, next->key) == 0) {
		last = next;
		next = next->next;
	}

	if (next && next->key && strcmp(key, next->key) == 0) {
		free(next->value);
		next->value = strdup(value);
	} else {
		newpair = hashtable_newpair(key, value);

		if (next == hashtable->table[bin]) {
			newpair->next = next;
			hashtable->table[bin] = newpair;
		} else if (!next) {
			last->next = newpair;
		} else {
			newpair->next = next;
			last->next = newpair;
		}
	}
	return;
}

char *hashtable_get (hashtable_t *hashtable, char *key)
{
	int bin = 0;
	hash_entry_t *pair;

	bin = hashtable_hash(hashtable, key);

	pair = hashtable->table[bin];
	while (pair && pair->key && strcmp(key, pair->key) > 0) {
		pair = pair->next;
	}

	if (!pair || !pair->key || strcmp(key, pair->key)) {
		return NULL;
	} else {
		return pair->value;
	}
	
	return NULL;
}

void hashtable_free (hashtable_t *hashtable)
{
	hash_entry_t *next, *last;
	next = last = NULL;
	for (int ii = 0; ii < hashtable->size; ii++) {
		next = hashtable->table[ii];
		while (next) {
			last = next;
			next = next->next;
			free(last);
		}
	}
}

const int PASS_LEN = 20;        // Maximum any password can be
const int HASH_LEN = 33;        // Length of MD5 hash strings

/*
 * Given a hash and a plaintext guess, return 1 if
 * the hash of the guess matches the given hash.
 * That is, return 1 if the guess is correct.
 */
int tryguess(char *hash, char *guess)
{
	// Hash the guess using MD5
	// Compare the two hashes
	// Free any malloc'd memory

	int result;
	char *guess_hash;
	if (!(guess_hash = malloc(sizeof(char) * HASH_LEN))) {
		fprintf(stderr, "Allocation Error\n");
		exit(5);
	}
	
	guess_hash = md5(guess, strlen(guess));
	
	result = strncmp(hash, guess_hash, sizeof(char) * HASH_LEN);
	
	free(guess_hash);
	return !result;
}

/*
 * Read in a file and return the array of strings.
 * Use the technique we showed in class to expand the
 * array as you go.
 */
char **readfile(char *filename)
{
	FILE *file;

	if(!(file = fopen(filename, "r"))) {
		fprintf(stderr, "Couldn't open file: %s\n", filename);
		exit(5);
	}

	struct stat fileinfo;
	stat(filename, &fileinfo);
	int filesize = fileinfo.st_size;

	char *raw;

	if (!(raw = malloc((filesize + 1) * sizeof(char)))) {
		fprintf(stderr, "Memory allocation error\n");
		exit(6);
	}

	fread(raw, 1, filesize, file);
	fclose(file);
	
	int lines = 0;
	for (int ii = 0; ii < filesize; ii++) {
		if ('\n' == raw[ii]) {
			raw[ii] = '\0';
			lines++;
		}
	}

	char **lineptr;
	if (!(lineptr = malloc(lines * sizeof(char *)))) {
		fprintf(stderr, "Memory allocation error\n");
		exit(7);
	}
	
	int jj = 1;

	lineptr[0] = raw;
	
	for (int ii = 0; ii < filesize - 1; ii++) {
		if ('\0' == raw[ii] && (ii + 1) < filesize - 1) {
			lineptr[jj++] = &raw[ii + 1];
		}
	}
		
	return lineptr;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.

    /*
     * Time trials:
     * wo/ ht: ~0.284s for 1000 passwords
     * w/ ht:  ~0.006s for 1000 passwords
     * Thats an improvement in my book :) Hashtables FTW
     */
    
    /*
     * Nested loops
     */
#if 0
    int ii, jj; ii = jj = 0;
    while (dict[ii]) {
	    jj = 0;
	    while(hashes[jj]) {
		    if (tryguess(hashes[jj], dict[ii])) {
			    printf("%s\n", dict[ii]);
		    }
		    jj++;
	    }
	    ii++;
    }    

    /*
     * Hash table
     */

#else
    int ii = 0;
    hashtable_t *hashtable = hashtable_create(1000);

    while (dict[ii]) {
	    hashtable_set(hashtable, md5(dict[ii], strlen(dict[ii])), dict[ii]);
	    ii++;
    }

    ii = 0;
    char *value;

    while (hashes[ii]) {
	    if ((value = hashtable_get(hashtable, hashes[ii]))) {
		    printf("%s\n", value);
	    }
	    ii++;
    }

    hashtable_free(hashtable);

    free(hashtable->table);
    free(hashtable);
#endif
    
    free(hashes[0]);
    free(hashes);
    free(dict[0]);
    free(dict);
    
    /* Compare hashed dict word to all hashes, not other way around */
}
